# List of parameters and their implementation: 

* $D_h$: mean distance between houses (visiting rate between houses is proportional to $1/d_h$)
    * network: "density within community", the mean density within each comunnities is $d_h$  (_ie_ there is an average $d_h$ link between individual of the same community.)?
    * spatial ABM: agent are limited to move at certain speeds within a radius $d_h$ from their point of birth. Each agent has a unique point of birth, point of birth are spaced with distance $d_v$.
* $D_v$: mean distance between villages (visiting rate between villages is proportional to $1/d_v$):
    * network: "density inter community", the mean density between comunnities is $d_v$  (_ie_ there is an average of $d_v$ links between two communities)
    * spatial ABM: a "small"  \% of agent are able to move at certain speed within a radius > $d_v$  (thus connecting villages). but then proba of inter village contact depends on: the "\% of agent", their speed, the radius within which they cna move and the distance between each senter
* $Sigma_h$: variance in inter-household distance
    * network: ? variance of the density within comunities
    * spatial: ?variance of the speed of the agents that stay within a circle?
* $Sigma_v$: variance in inter-village distance
    * network: ? variance of the density between comunities  
    * spatial: ?variance of the speed of the agents that moves everywher
* $N_h$: Number of houses per village 
    * network: number of node per communities
    * spatial abm: number agents within each radius
* $I(d, t)$  – probability of infection, which depends on distance, d (inversely proportional to probability of contact),  and time, t, spent in contact
    * network: not sure yet how to integrate the temporal aspect maybe at each time step all node interact only with a subset of the node they are connected with. Thus they can us the number of consecutive timestep when they were in contact with infected people as the $t$ (I think matt uses something like that ine the model).
    * spatial abm: can be just a probability of infection when to agents are close enought. the $d$ part emerges from the speed/radius/initial distances of the agents. To implement $t$, one way could be to add a counter: when an agent $i$ encounter an agent infected $j$, thus the counter start. Then this counter coudl increase if $i$ meet again $j$ in the next time step, or another infected agents. and we could have somethin like $t(c,ni)$ where $ni$ count is the number of infected people and $c$ the number of time step i tooks to meet this number of infected people.
        

